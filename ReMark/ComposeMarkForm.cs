﻿// This file is part of ReMark.
// ReMark is free software developed by LezSoft. While LezSoft holds all rights on the ReMark brand and logo,
// you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
// Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
// 
// SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
// SPDX-License-Identifier: GPL-3.0-or-later

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace ReMark
{ 
    public partial class ComposeMarkForm : System.Windows.Forms.Form
    {
        #region Fields

        UIDocument uiDoc = null;
        Document doc;

        List<Element> elements;
        List<string> categories;

        List<List<Element>> elementLists;

        Dictionary<string, string[]> categoryPrefSuf = new Dictionary<string, string[]>();

        // get document levels and update the data in DataKeeper class
        Dictionary<ElementId, int> levels = new Dictionary<ElementId, int>();

        #endregion

        #region Constructor

        public ComposeMarkForm(UIDocument uIDocument)
        {
            uiDoc = uIDocument;
            InitializeComponent();
        }

        #endregion

        #region Methods

        private void ComposeMarkForm_Load(object sender, EventArgs e)
        {
            // get the current document
            doc = uiDoc.Document;

            // store all elements in the document
            elements = GetAllElements(doc);

            // store all used categories in a list
            categories = GetCategories(elements);
            categories.Sort();

            // add all categories to the categories dataTable in the form
            foreach (string category in categories)
            {
                if (DataKeeper.CategoryPrefSuf.ContainsKey(category))
                {
                    categoryInfosBindingSource.Add(new CategoryInfos(category, DataKeeper.CategoryPrefSuf[category][0], DataKeeper.CategoryPrefSuf[category][1], bool.Parse(DataKeeper.CategoryPrefSuf[category][2])));
                }
                else
                    categoryInfosBindingSource.Add(new CategoryInfos(category));
            }

            // sort elements by category
            elementLists = SortElements(elements, categories);
        }

        /// <summary>
        /// // store all elements in the document into a list (store the element only if it has a category and a Mark parameter, it has a level id and a geometry, in order to exclude non-object elements)
        /// </summary>
        /// <param name="doc">the document</param>
        /// <returns>the list of elements in the document</returns>
        public List<Element> GetAllElements(Document doc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(doc).WhereElementIsNotElementType();
            List<Element> elements = new List<Element>();
            foreach (Element element in collector)
            {
                if (null != element.Category
                    && null != element.LevelId
                    && null != element.get_Geometry(doc.Application.Create.NewGeometryOptions())
                    && null != element.get_Parameter(BuiltInParameter.ALL_MODEL_MARK))
                {
                    elements.Add(element);
                }
            }

            return elements;
        }

        /// <summary>
        /// Get all the categories of objects used in the document
        /// </summary>
        /// <param name="elements">list of elements in the document</param>
        /// <returns>list of the categories used in the document</returns>
        public List<string> GetCategories(List<Element> elements)
        {
            List<string> categories = new List<string>();

            foreach (Element element in elements)
            {
                string category = element.Category.Name;
                if (!categories.Contains(category))
                {
                    categories.Add(category);
                }
            }

            return categories;
        }

        /// <summary>
        /// Sort given elements based on their categories
        /// </summary>
        /// <param name="elements">the elements to sort</param>
        /// <param name="categories">the categories used for the sorting operation</param>
        /// <returns>a list which contains a list of element for every category</returns>
        public List<List<Element>> SortElements(List<Element> elements, List<string> categories)
        {
            List<List<Element>> elementLists = new List<List<Element>>();

            foreach (string category in categories)
            {
                elementLists.Add(new List<Element>());
            }
            foreach (Element element in elements)
            {
                for (int i = 0; i < categories.Count; i++)
                {
                    if (element.Category.Name == categories[i])
                    {
                        elementLists[i].Add(element);
                    }
                }
            }

            return elementLists;
        }

        /// <summary>
        /// Handle Apply Button Click
        /// </summary>
        private void Apply_Btn_Click(object sender, EventArgs e)
        {
            foreach (CategoryInfos category in categoryInfosBindingSource)
            {
                if (categoryPrefSuf.ContainsKey(category.Category))
                {
                    categoryPrefSuf.Remove(category.Category);
                }
                categoryPrefSuf.Add(category.Category, new string[] { category.Prefix, category.Suffix, category.Checked.ToString() });
            }
            // update the old dictionary stored in the DataKeeper Class
            DataKeeper.CategoryPrefSuf = categoryPrefSuf;

            // update the old value for starting level stored in the DataKeeper Class
            DataKeeper.StartingLevel = int.Parse(starting_level_TBx.Text);

            // store all document levels in a dictionary keyed by level id
            levels = GetDocumentLevels(doc, DataKeeper.StartingLevel);
            // update the old dictionary stored in the DataKeeper class
            DataKeeper.Levels = levels;

            // rename all marks
            GenerateNewMarks();

            // show a success dialog
            TaskDialog.Show("Success", Properties.Resources.rename_success_message);
        }

        /// <summary>
        /// Generate new marks based on users' input
        /// </summary>
        public void GenerateNewMarks()
        {
            foreach (List<Element> elementList in elementLists)
            {
                // start a counter for elements of this category
                int counter = 1;

                // store current category name, prefix and suffix
                string currentCategory = elementList[0].Category.Name;
                string categoryPrefix = categoryPrefSuf[currentCategory][0];
                string categorySuffix = categoryPrefSuf[currentCategory][1];
                bool categoryCheck = bool.Parse(categoryPrefSuf[currentCategory][2]);

                // modify marks only for checked categories
                if (categoryCheck == true)
                {
                    // change all elements' mark
                    foreach (Element element in elementList)
                    {
                        // get the mark parameter of the element
                        Parameter mark = element.get_Parameter(BuiltInParameter.ALL_MODEL_MARK);

                        // compose the new Mark based on user's input
                        StringBuilder newMark = new StringBuilder();
                        if (categoryPrefix != null)
                        {
                            newMark.Append($"{categoryPrefix}.");
                        }
                        if (level_CBx.Checked && element.LevelId.IntegerValue != -1)
                        {
                            newMark.Append($"{levels[element.LevelId]}.");
                        }
                        newMark.Append(counter);
                        if (categorySuffix != null)
                        {
                            newMark.Append($".{categorySuffix}");
                        }

                        // set the new mark
                        using (Transaction t = new Transaction(doc, "Change Marks"))
                        {
                            t.Start();
                            try
                            {
                                mark.Set(newMark.ToString());
                            }
                            catch { }
                            t.Commit();
                        }

                        // increment the element counter
                        counter++;
                    } 
                }
            }
        }

        /// <summary>
        /// Handle Cancel Button Click
        /// </summary>
        private void cancel_Btn_Click(object sender, EventArgs e)
        {
            // update the local dictionary if needed
            foreach (CategoryInfos category in categoryInfosBindingSource)
            {
                try
                {
                    categoryPrefSuf.Add(category.Category, new string[] { category.Prefix, category.Suffix, category.Checked.ToString() });
                }
                catch { } // if the dictionary is already updated it will throw and exception, so just ignore it
            }
            // update the old dictionary stored in the DataKeeper Class
            DataKeeper.CategoryPrefSuf = categoryPrefSuf;

            // update the old value for starting level stored in the DataKeeper Class
            DataKeeper.StartingLevel = int.Parse(starting_level_TBx.Text);

            this.Close();
        }

        /// <summary>
        /// Get the levels in the given document
        /// </summary>
        /// <param name="doc">the document</param>
        /// <param name="startingLevel">the lowest level</param>
        /// <returns>a dictionary of level numbers keyed by level ids</returns>
        public Dictionary<ElementId, int> GetDocumentLevels(Document doc, int startingLevel)
        {
            // create a dictionary of level number keyed by level id
            Dictionary<ElementId, int> levels = new Dictionary<ElementId, int>();

            // collect all document levels and store them in a list
            FilteredElementCollector levelsCollector = new FilteredElementCollector(doc).OfClass(typeof(Level));
            List<Level> levelsList = new List<Level>();
            foreach (Level level in levelsCollector)
            {
                levelsList.Add(level);
            }

            // sort the list (to have levels in order)
            levelsList.Sort(LevelComparison);

            // store levels in the dictionary
            foreach (Level level in levelsList)
            {
                levels.Add(level.Id, startingLevel);
                startingLevel++;
            }

            // return the dictionary
            return levels;
        }

        public int LevelComparison(Level level1, Level level2)
        {
            return level1.Elevation.CompareTo(level2.Elevation);
        }

        /// <summary>
        /// Handle the change of checked status of the level checkbox
        /// </summary>
        private void level_CBx_CheckedChanged(object sender, EventArgs e)
        {
            if (level_CBx.Checked)
            {
                starting_level_Lbl.Visible = true;
                starting_level_TBx.Visible = true;
                starting_level_TBx.Enabled = true;
                starting_level_TBx.Text = DataKeeper.StartingLevel.ToString();
            }
            else
            {
                starting_level_Lbl.Visible = false;
                starting_level_TBx.Visible = false;
                starting_level_TBx.Enabled = false;
            }

        }

        /// <summary>
        /// Handle Check All Button Click
        /// </summary>
        private void checkAll_Btn_Click(object sender, EventArgs e)
        {
            // change check status in the data grid bindingSource
            foreach (CategoryInfos category in categoryInfosBindingSource)
            {
                category.Checked = true;
            }
            // refresh the data grid
            categories_DGV.Refresh();
        }

        /// <summary>
        /// Handle Uncheck All Button Click
        /// </summary>
        private void uncheckAll_Btn_Click(object sender, EventArgs e)
        {
            // change check status in the data grid bindingSource
            foreach (CategoryInfos category in categoryInfosBindingSource)
            {
                category.Checked = false;
            }
            // refresh the data grid
            categories_DGV.Refresh();
        }

        /// <summary>
        /// Handle when the user leave the starting level textbox
        /// </summary>
        private void starting_level_TBx_Leave(object sender, EventArgs e)
        {
            try
            {
                int.Parse(starting_level_TBx.Text);
            }
            catch (FormatException)
            {
                starting_level_TBx.Text = "0";
                TaskDialog.Show("Error", Properties.Resources.starting_level_error_message);
            }
        }

        #endregion
    }
}