﻿// This file is part of ReMark.
// ReMark is free software developed by LezSoft. While LezSoft holds all rights on the ReMark brand and logo,
// you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
// Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
// 
// SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
// SPDX-License-Identifier: GPL-3.0-or-later

using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReMark
{
    public static class DataKeeper
    {
        static Dictionary<string, string[]> categoryPrefSuf = new Dictionary<string, string[]>();
        static Dictionary<ElementId, int> levels = new Dictionary<ElementId, int>();
        static int startingLevel = 0;

        /// <summary>
        /// A propriety for the stored Categories' Prefixes and Suffixes
        /// </summary>
        public static Dictionary<string, string[]> CategoryPrefSuf
        {
            get { return categoryPrefSuf; }
            set { categoryPrefSuf = value; }
        }

        /// <summary>
        /// A propriety for the stored Document's levels
        /// </summary>
        public static Dictionary<ElementId, int> Levels
        {
            get { return levels; }
            set { levels = value; }
        }

        /// <summary>
        /// A propriety for the stored starting level
        /// </summary>
        public static int StartingLevel
        {
            get { return startingLevel; }
            set { startingLevel = value; }
        }
    }
}