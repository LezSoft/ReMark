﻿// This file is part of ReMark.
// ReMark is free software developed by LezSoft. While LezSoft holds all rights on the ReMark brand and logo,
// you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
// Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
// 
// SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
// SPDX-License-Identifier: GPL-3.0-or-later

using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ReMark
{
    class App : IExternalApplication
    {
        const string MyRibbonTab = "LezTools";
        const string MyRibbonPanel = "ReMark";

        const string HelpPageUrl = "https://lezsoft.com/remark/";


        public Result OnStartup(UIControlledApplication application)
        {
            // create the Ribbon Tab (if it doesn't exist)
            try
            {
                application.CreateRibbonTab(MyRibbonTab);
            }
            catch (Exception) { } // if there is an exception, the tab already exists

            //get the Panel
            RibbonPanel panel = null;
            List<RibbonPanel> panels = application.GetRibbonPanels(MyRibbonTab);
            foreach (RibbonPanel p in panels)
            {
                if (p.Name == MyRibbonPanel)
                {
                    panel = p;
                    break;
                }
            }

            // create the pannel if it doesn't already exist
            if (panel == null)
            {
                panel = application.CreateRibbonPanel(MyRibbonTab, MyRibbonPanel);
            }

            // get the Rename button icons
            Image renameBtn_img16 = Properties.Resources.Rename_Marks_Icon_16;
            Image renameBtn_img32 = Properties.Resources.Rename_Marks_Icon_32;

            // get the Delete button icons
            Image deleteBtn_img16 = Properties.Resources.Delete_Marks_Icon_16;
            Image deleteBtn_img32 = Properties.Resources.Delete_Marks_Icon_32;

            // create the Rename Button data
            PushButtonData renameBtnData = new PushButtonData(
                "RenameMarksButton",
                Properties.Resources.renameBtnName,
                Assembly.GetExecutingAssembly().Location,
                "ReMark.Command")
            {
                ToolTip = Properties.Resources.renameBtnToolTip,
                LongDescription = Properties.Resources.renameBtnLongDescription,
                Image = GetImageSource(renameBtn_img16),
                LargeImage = GetImageSource(renameBtn_img32)
            };

            // create the Delete Button data
            PushButtonData deleteBtnData = new PushButtonData(
                "DeleteMarksButton",
                Properties.Resources.deleteBtnName,
                Assembly.GetExecutingAssembly().Location,
                "ReMark.DeleteMarksCommand")
            {
                ToolTip = Properties.Resources.deleteBtnToolTip,
                LongDescription = Properties.Resources.deleteBtnLongDescription,
                Image = GetImageSource(deleteBtn_img16),
                LargeImage = GetImageSource(deleteBtn_img32)
            };

            // add the buttons to the panel
            PushButton renameBtn = panel.AddItem(renameBtnData) as PushButton;
            PushButton deleteBtn = panel.AddItem(deleteBtnData) as PushButton;

            // create F1-Help reference and add it to the buttons
            ContextualHelp contextualHelp = new ContextualHelp(ContextualHelpType.Url, HelpPageUrl);
            renameBtn.SetContextualHelp(contextualHelp);
            deleteBtn.SetContextualHelp(contextualHelp);

            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }

        /// <summary>
        /// Get the BitmapSource of an image (code pasted from https://youtu.be/lYjHDkpbsas?t=747)
        /// </summary>
        /// <param name="img">The Image</param>
        /// <returns>The BitmapSource of the given image</returns>
        BitmapSource GetImageSource(Image img)
        {
            BitmapImage bmp = new BitmapImage();

            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, ImageFormat.Png);
                ms.Position = 0;

                bmp.BeginInit();

                bmp.CacheOption = BitmapCacheOption.OnLoad;
                bmp.UriSource = null;
                bmp.StreamSource = ms;

                bmp.EndInit();
            }

            return bmp;
        }
    }
}
