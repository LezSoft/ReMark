﻿// This file is part of ReMark.
// ReMark is free software developed by LezSoft. While LezSoft holds all rights on the ReMark brand and logo,
// you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
// Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
// 
// SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
// SPDX-License-Identifier: GPL-3.0-or-later

using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReMark
{
    [Transaction(TransactionMode.Manual)]
    class DeleteMarksCommand : IExternalCommand
    {
        Document doc;

        List<Element> elements = new List<Element>();

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            doc = commandData.Application.ActiveUIDocument.Document;

            this.elements = GetAllElements(doc);

            DeleteMarks(doc, this.elements);

            TaskDialog.Show("Success", Properties.Resources.delete_success_message);

            return Result.Succeeded;
        }

        /// <summary>
        /// // store all elements in the document into a list (store the element only if it has a category and a Mark parameter, it has a level id and a geometry, in order to exclude non-object elements)
        /// </summary>
        /// <param name="doc">the document</param>
        /// <returns>the list of elements in the document</returns>
        public List<Element> GetAllElements(Document doc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(doc).WhereElementIsNotElementType();
            List<Element> elements = new List<Element>();
            foreach (Element element in collector)
            {
                if (null != element.Category
                    && null != element.LevelId
                    && null != element.get_Geometry(doc.Application.Create.NewGeometryOptions())
                    && null != element.get_Parameter(BuiltInParameter.ALL_MODEL_MARK))
                {
                    elements.Add(element);
                }
            }

            return elements;
        }

        /// <summary>
        /// Delete the current mark for each given element in the document
        /// </summary>
        /// <param name="doc">the document</param>
        /// <param name="elements">the list of elements which marks wil be deleted</param>
        public void DeleteMarks(Document doc, List<Element> elements)
        {
            foreach (Element element in elements)
            {
                Parameter mark = element.get_Parameter(BuiltInParameter.ALL_MODEL_MARK);

                using (Transaction t = new Transaction(doc, "Delete Elements Marks"))
                {
                    t.Start();
                    try
                    {
                        mark.Set("");
                    }
                    catch { }
                    t.Commit();
                }
            }
        }
    }
}
