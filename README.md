<!--
    This file is part of ReMark.
    ReMark is free software developed by LezSoft. While LezSoft holds all rights on the ReMark brand and logo,
    you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
    Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.

    SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
    SPDX-License-Identifier: GPL-3.0-or-later
-->

# [ReMark](https://lezsoft.com/remark)

### ReadMe Navigation:
1. [What is *ReMark*](#what-is-remark)
2. [Features](#features)
3. [Contributing Guidelines](#contributing-guidelines)
4. [Copyright and License](#copyright-and-license)

---
### What is *ReMark*
*ReMark* is a plugin for Autodesk Revit© that lets you easily add a unique custom mark to selected categories' instances.

It's [available on the Autodesk App Store©](https://apps.autodesk.com/RVT/en/Detail/Index?id=2217647408744317432&appLang=en&os=Win64) and it's compatible with all Revit© versions after the 2020 one.

---
### Features:
The plugin will add 2 buttons to our custom ribbon tab (`LezTools`):

* The `Rename Marks` button that opens a form where you'll find the following options:
  * `Check all Categories` - check all categories' checkboxes
  * `Uncheck all Categories` - uncheck all categories' checkboxes
  * `Include Level Number` - choose to include the level number in the new mark by typing the number of the lower level of the project
  * `Cancel` - close the *Rename Marks* form
  * `Apply` - rename the marks of the selected categories' instances with the specified criteria
* The `Delete All Marks` button that deletes the mark of every instance in the project

---
### Contributing Guidelines
We appreciate all contributions and Pull Requests since we think that cooperation between developers and users is a core part of open-source. However, to ensure that the new code works flawlessly with the old one and share its "style", all Pull Requests will be reviewed by a LezSoft developer before being merged.

If you have any non-code suggestion please open an Issue here on Codeberg so that it will be discussed togheter with all the community.

If you have any question feel free to reach us [by email](mailto:info@lezsoft.com)

---
### Copyright and License
This project has been developed by [LezSoft](https://lezsoft.com). All the rights on the *ReMark* brand (with its name and logo) are reserved.

*ReMark* is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

*ReMark* is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
