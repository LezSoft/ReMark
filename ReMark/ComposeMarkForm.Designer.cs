﻿namespace ReMark
{
    partial class ComposeMarkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComposeMarkForm));
            this.Apply_Btn = new System.Windows.Forms.Button();
            this.level_CBx = new System.Windows.Forms.CheckBox();
            this.cancel_Btn = new System.Windows.Forms.Button();
            this.starting_level_TBx = new System.Windows.Forms.TextBox();
            this.starting_level_Lbl = new System.Windows.Forms.Label();
            this.categories_DGV = new System.Windows.Forms.DataGridView();
            this.checkedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.categoryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prefixDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.suffixDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryInfosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkAll_Btn = new System.Windows.Forms.Button();
            this.uncheckAll_Btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.categories_DGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryInfosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Apply_Btn
            // 
            resources.ApplyResources(this.Apply_Btn, "Apply_Btn");
            this.Apply_Btn.Name = "Apply_Btn";
            this.Apply_Btn.UseVisualStyleBackColor = true;
            this.Apply_Btn.Click += new System.EventHandler(this.Apply_Btn_Click);
            // 
            // level_CBx
            // 
            resources.ApplyResources(this.level_CBx, "level_CBx");
            this.level_CBx.Name = "level_CBx";
            this.level_CBx.UseVisualStyleBackColor = true;
            this.level_CBx.CheckedChanged += new System.EventHandler(this.level_CBx_CheckedChanged);
            // 
            // cancel_Btn
            // 
            resources.ApplyResources(this.cancel_Btn, "cancel_Btn");
            this.cancel_Btn.Name = "cancel_Btn";
            this.cancel_Btn.UseVisualStyleBackColor = true;
            this.cancel_Btn.Click += new System.EventHandler(this.cancel_Btn_Click);
            // 
            // starting_level_TBx
            // 
            resources.ApplyResources(this.starting_level_TBx, "starting_level_TBx");
            this.starting_level_TBx.Name = "starting_level_TBx";
            this.starting_level_TBx.Leave += new System.EventHandler(this.starting_level_TBx_Leave);
            // 
            // starting_level_Lbl
            // 
            resources.ApplyResources(this.starting_level_Lbl, "starting_level_Lbl");
            this.starting_level_Lbl.Name = "starting_level_Lbl";
            // 
            // categories_DGV
            // 
            resources.ApplyResources(this.categories_DGV, "categories_DGV");
            this.categories_DGV.AutoGenerateColumns = false;
            this.categories_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.categories_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.categories_DGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.checkedDataGridViewCheckBoxColumn,
            this.categoryDataGridViewTextBoxColumn,
            this.prefixDataGridViewTextBoxColumn,
            this.suffixDataGridViewTextBoxColumn});
            this.categories_DGV.DataSource = this.categoryInfosBindingSource;
            this.categories_DGV.Name = "categories_DGV";
            // 
            // checkedDataGridViewCheckBoxColumn
            // 
            this.checkedDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.checkedDataGridViewCheckBoxColumn.DataPropertyName = "Checked";
            this.checkedDataGridViewCheckBoxColumn.FillWeight = 98.39572F;
            resources.ApplyResources(this.checkedDataGridViewCheckBoxColumn, "checkedDataGridViewCheckBoxColumn");
            this.checkedDataGridViewCheckBoxColumn.Name = "checkedDataGridViewCheckBoxColumn";
            // 
            // categoryDataGridViewTextBoxColumn
            // 
            this.categoryDataGridViewTextBoxColumn.DataPropertyName = "Category";
            this.categoryDataGridViewTextBoxColumn.FillWeight = 134.6448F;
            resources.ApplyResources(this.categoryDataGridViewTextBoxColumn, "categoryDataGridViewTextBoxColumn");
            this.categoryDataGridViewTextBoxColumn.Name = "categoryDataGridViewTextBoxColumn";
            this.categoryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // prefixDataGridViewTextBoxColumn
            // 
            this.prefixDataGridViewTextBoxColumn.DataPropertyName = "Prefix";
            this.prefixDataGridViewTextBoxColumn.FillWeight = 83.47979F;
            resources.ApplyResources(this.prefixDataGridViewTextBoxColumn, "prefixDataGridViewTextBoxColumn");
            this.prefixDataGridViewTextBoxColumn.Name = "prefixDataGridViewTextBoxColumn";
            // 
            // suffixDataGridViewTextBoxColumn
            // 
            this.suffixDataGridViewTextBoxColumn.DataPropertyName = "Suffix";
            this.suffixDataGridViewTextBoxColumn.FillWeight = 83.47979F;
            resources.ApplyResources(this.suffixDataGridViewTextBoxColumn, "suffixDataGridViewTextBoxColumn");
            this.suffixDataGridViewTextBoxColumn.Name = "suffixDataGridViewTextBoxColumn";
            // 
            // categoryInfosBindingSource
            // 
            this.categoryInfosBindingSource.DataSource = typeof(ReMark.CategoryInfos);
            // 
            // checkAll_Btn
            // 
            resources.ApplyResources(this.checkAll_Btn, "checkAll_Btn");
            this.checkAll_Btn.Name = "checkAll_Btn";
            this.checkAll_Btn.UseVisualStyleBackColor = true;
            this.checkAll_Btn.Click += new System.EventHandler(this.checkAll_Btn_Click);
            // 
            // uncheckAll_Btn
            // 
            resources.ApplyResources(this.uncheckAll_Btn, "uncheckAll_Btn");
            this.uncheckAll_Btn.Name = "uncheckAll_Btn";
            this.uncheckAll_Btn.UseVisualStyleBackColor = true;
            this.uncheckAll_Btn.Click += new System.EventHandler(this.uncheckAll_Btn_Click);
            // 
            // ComposeMarkForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uncheckAll_Btn);
            this.Controls.Add(this.checkAll_Btn);
            this.Controls.Add(this.categories_DGV);
            this.Controls.Add(this.starting_level_Lbl);
            this.Controls.Add(this.starting_level_TBx);
            this.Controls.Add(this.cancel_Btn);
            this.Controls.Add(this.level_CBx);
            this.Controls.Add(this.Apply_Btn);
            this.MaximizeBox = false;
            this.Name = "ComposeMarkForm";
            this.Load += new System.EventHandler(this.ComposeMarkForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.categories_DGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryInfosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Apply_Btn;
        private System.Windows.Forms.CheckBox level_CBx;
        private System.Windows.Forms.Button cancel_Btn;
        private System.Windows.Forms.TextBox starting_level_TBx;
        private System.Windows.Forms.Label starting_level_Lbl;
        private System.Windows.Forms.BindingSource categoryInfosBindingSource;
        private System.Windows.Forms.DataGridView categories_DGV;
        private System.Windows.Forms.Button checkAll_Btn;
        private System.Windows.Forms.Button uncheckAll_Btn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checkedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prefixDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn suffixDataGridViewTextBoxColumn;
    }
}